﻿using System;
using System.Collections.Generic;

namespace WeightedStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputString = "abbcccd";
            int[] queries = { 1, 3, 9, 8 };

            List<string> result = DetermineStatus(inputString, queries);

            Console.WriteLine("Output:");
            Console.WriteLine(string.Join(", ", result));

            // Menunggu hingga pengguna menekan tombol apa pun sebelum keluar
            Console.WriteLine("\nTekan tombol apa pun untuk keluar...");
            Console.ReadKey();
        }

        static List<string> DetermineStatus(string inputString, int[] queries)
        {
            Dictionary<char, int> weights = new Dictionary<char, int>
            {
                { 'a', 1 }, { 'b', 2 }, { 'c', 3 }, { 'd', 4 }
                // Tambahkan karakter dan bobotnya sesuai kebutuhan
            };

            List<string> result = new List<string>();

            foreach (int query in queries)
            {
                bool found = false;
                for (int i = 0; i < inputString.Length; i++)
                {
                    int substringWeight = 0;
                    for (int j = i; j < inputString.Length; j++)
                    {
                        substringWeight += weights[inputString[j]];
                        if (substringWeight == query)
                        {
                            found = true;
                            break;
                        }
                        else if (substringWeight > query)
                        {
                            break;
                        }
                    }
                    if (found)
                    {
                        break;
                    }
                }
                result.Add(found ? "Yes" : "No");
            }

            return result;
        }
    }
}
