﻿using System;
using System.Collections.Generic;

namespace HighestPalindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            string input1 = "3943";
            int k1 = 1;
            Console.WriteLine($"Sample 1: {GetHighestPalindrome(input1, k1)}"); // Output: 3993

            string input2 = "3943";
            int k2 = 2;
            Console.WriteLine($"Sample 2: {GetHighestPalindrome(input2, k2)}"); // Output: 992299

            // Menunggu hingga pengguna menekan tombol apa pun sebelum keluar
            Console.WriteLine("\nTekan tombol apa pun untuk keluar...");
            Console.ReadKey();
        }

        static string GetHighestPalindrome(string input, int k)
        {
            return GetHighestPalindromeHelper(input, 0, input.Length - 1, k);
        }

        static string GetHighestPalindromeHelper(string input, int left, int right, int k)
        {
            if (left >= right)
                return input;

            if (input[left] != input[right])
            {
                char maxChar = (char)Math.Max(input[left], input[right]);
                input = input.Substring(0, left) + maxChar + input.Substring(left + 1);
                input = input.Substring(0, right) + maxChar + input.Substring(right + 1);
                k--;
            }

            if (k < 0)
                return "-1";

            return GetHighestPalindromeHelper(input, left + 1, right - 1, k);
        }
    }
}
